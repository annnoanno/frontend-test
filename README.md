# AnnoAnno-frontend-test

## Task Description

### UI requirements

Build a vue component **'ProductCard.vue'**
The UI of the component is presented in 01.png and 02.png in the root directory.

https://annoanno.dk/produkt-kategori/ 
[note] Attached image has been slightly modified for testing purposes.

The Product Card component should get `product` object as a prop.
`product` object typically has a structure of
```
{
    "id": 43169,
    "sku": "JR21007660",
    "url": "",
    "slug": "slug",
    "category_id": 413,
    "subcategory_id": 157,
    "material": "material information",
    "color": {
      "id": 64,
      "name": "Blue"
    },
    "brand": {
      "name": "Junarose"
    },
    "images": {
      "thumbnail": "https://media.annoanno.dk/img/thumbnails/styling/21466-186407-95035-43604.jpg"
    },
    "price": {
      "currency": "DKK",
      "currency_id": 1,
      "regular_price": 199,
      "sales_price": 99,
      "purchase_price": 199,
      "discount_percentage": 50
    },
    "variants": {
      "54": {
        "id": 43176,
        "size": "54",
        "converted_size_range": [
          54,
          54
        ],
        "sku": "sku2342",
        "ean": "123456789"
      }
    },
    "tags": []
  }
  ```
  but not all these information is displayed. 
  Only brand name, regular and discount price, discount percentage and product image is used on the actual ui. 


### API integration and currency conversion

We have prepared 2 mock apis.
- https://run.mocky.io/v3/f97d084a-25db-48eb-9b0b-2261f32f233c
- https://run.mocky.io/v3/1d00abe2-86ea-4d77-b449-38c6ad890bc3

All fields for products are same, but currency is different. Based on the currency, product card should show either `*  kr.` or `€ *` format of price.
When run on local environment, based on the port number it should switch the API endpoint.
For example, when project is running on **3000** port, it should call first endpoint, which returns DKK price. 
When running on **3001** port, it should call the second endpoint, which returns EUR price.

[Optional Points]
- You can simply copy the API response from each API endpoints and keep it as a mock data in json file and import it in `App.vue` file instead of calling the API. 
- Using `v-if` for currency format is fine, but it will be a bonus if
     in `ProductCard.vue` component, create a [vue filter](https://vuejs.org/v2/guide/filters.html) that accepts price object and returns correct currency format.


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
